/**
 * Created by saurabh on 10/02/17.
 */
var express = require('express'),csv = require('express-csv');
var router = express.Router();
var MongoClient = require('mongodb').MongoClient;
var dbURI = 'mongodb://localhost/IRS';
var csv_export=require('csv-export');

router.get('/export/:state', function (req, res, next) {
    console.log("export")
    state=req.params.state
    state=state.toUpperCase()
    var json2csv = require('json2csv');
    var fs = require('fs');
    csv_data=[]
    MongoClient.connect(dbURI, function (err, db) {
            if (err) {
                throw err
            }

            var cursor = db.collection('np990').find({"Return.ReturnHeader.Filer.USAddress.StateAbbreviationCd": state}, {})
            cursor.toArray(function (err, doc) {
                if (err) throw err
                if (doc != null) {
                    for (var i = 0; i < doc.length; i++) {
                        console.log(state)
                        var objdetail = {}
                        objdetail['id'] = doc[i]._id

                        objdetail['ein'] = doc[i].Return.ReturnHeader.Filer.EIN
                        try {
                            objdetail['name'] = doc[i].Return.ReturnHeader.Filer.BusinessName.BusinessNameLine1Txt

                        } catch (err) {
                            try {
                                objdetail['name'] = doc[i].info.BusinessName.BusinessNameLine1

                            } catch (err) {
                                objdetail['name'] = doc[i].info.Name.BusinessNameLine1

                            }

                        }
                        if (doc[i].Return.ReturnHeader.Filer.USAddress.AddressLine1Txt == undefined)
                            objdetail['addressLine1'] = doc[i].Return.ReturnHeader.Filer.USAddress.AddressLine1Txt
                        else
                            objdetail['addressLine1'] = doc[i].Return.ReturnHeader.Filer.USAddress.AddressLine1Txt
                        if (objdetail['cityName'] = doc[i].Return.ReturnHeader.Filer.USAddress.City == undefined)
                            objdetail['cityName'] = doc[i].Return.ReturnHeader.Filer.USAddress.CityNm
                        else
                            objdetail['cityName'] = doc[i].Return.ReturnHeader.Filer.USAddress.City
                        if (objdetail['state'] = doc[i].Return.ReturnHeader.Filer.USAddress.StateAbbreviationCd == undefined)
                            objdetail['state'] = doc[i].Return.ReturnHeader.Filer.USAddress.StateAbbreviationCd
                        else
                            objdetail['state'] = doc[i].Return.ReturnHeader.Filer.USAddress.StateAbbreviationCd
                        if (objdetail['zip'] = doc[i].Return.ReturnHeader.Filer.USAddress.ZIPCd == undefined)
                            objdetail['zip'] = doc[i].Return.ReturnHeader.Filer.USAddress.ZIPCd
                        else
                            objdetail['zip'] = doc[i].Return.ReturnHeader.Filer.USAddress.ZIPCd
                        /*objdetail['website'] = doc[i].website
                         objdetail['expenseAmt'] = doc[i].expenseAmt
                         objdetail['taxYear'] = doc[i].taxYear
                         objdetail['rulingDate'] = doc[i].rulingdate
                         objdetail['missionDescription'] = doc[i].missionDescription*/
                        objdetail['returnType'] = doc[i].Return.ReturnHeader.returnType
                        objdetail['businessOfficerPersonName'] = doc[i].Return.ReturnHeader.BusinessOfficerGrp.PersonNm
                        objdetail['businessOfficerPersonTitle'] = doc[i].Return.ReturnHeader.BusinessOfficerGrp.PersonTitleTxt
                        objdetail['businessOfficerPersonPhone'] = doc[i].Return.ReturnHeader.BusinessOfficerGrp.PhoneNum
                        objdetail['businessOfficerPersonSignatureDate'] = doc[i].Return.ReturnHeader.BusinessOfficerGrp.SignatureDt
                        objdetail['EmployeeCnt'] = doc[i].Return.ReturnData.IRS990.EmployeeCnt
                        objdetail['TotalVolunteersCnt'] = doc[i].Return.ReturnData.IRS990.TotalVolunteersCnt
                        objdetail['PYTotalRevenueAmt'] = doc[i].Return.ReturnData.IRS990.PYTotalRevenueAmt
                        objdetail['CYTotalRevenueAmt'] = doc[i].Return.ReturnData.IRS990.CYTotalRevenueAmt
                        objdetail['CYTotalFundraisingExpenseAmt'] = doc[i].Return.ReturnData.IRS990.CYTotalFundraisingExpenseAmt
                        objdetail['PYContributionsGrantsAmt'] = doc[i].Return.ReturnData.IRS990.PYContributionsGrantsAmt
                        objdetail['CYContributionsGrantsAmt'] = doc[i].Return.ReturnData.IRS990.CYContributionsGrantsAmt
                        objdetail['PYProgramServiceRevenueAmt'] = doc[i].Return.ReturnData.IRS990.PYProgramServiceRevenueAmt
                        objdetail['CYProgramServiceRevenueAmt'] = doc[i].Return.ReturnData.IRS990.CYProgramServiceRevenueAmt
                        objdetail['PYInvestmentIncomeAmt'] = doc[i].Return.ReturnData.IRS990.PYInvestmentIncomeAmt
                        objdetail['CYInvestmentIncomeAmt'] = doc[i].Return.ReturnData.IRS990.CYInvestmentIncomeAmt
                        objdetail['PYOtherRevenueAmt'] = doc[i].Return.ReturnData.IRS990.PYOtherRevenueAmt
                        objdetail['CYOtherRevenueAmt'] = doc[i].Return.ReturnData.IRS990.CYOtherRevenueAmt
                        objdetail['PYSalariesCompEmpBnftPaidAmt'] = doc[i].Return.ReturnData.IRS990.PYSalariesCompEmpBnftPaidAmt
                        objdetail['CYSalariesCompEmpBnftPaidAmt'] = doc[i].Return.ReturnData.IRS990.CYSalariesCompEmpBnftPaidAmt
                        objdetail['PYTotalProfFndrsngExpnsAmt'] = doc[i].Return.ReturnData.IRS990.PYTotalProfFndrsngExpnsAmt
                        objdetail['CYTotalProfFndrsngExpnsAmt'] = doc[i].Return.ReturnData.IRS990.CYTotalProfFndrsngExpnsAmt
                        objdetail['CYTotalFundraisingExpenseAmt'] = doc[i].Return.ReturnData.IRS990.CYTotalFundraisingExpenseAmt
                        objdetail['CYTotalFundraisingExpenseAmt'] = doc[i].Return.ReturnData.IRS990.CYTotalFundraisingExpenseAmt
                        objdetail['missionDescription'] = doc[i].Return.ReturnData.IRS990.MissionDesc
                        objdetail['ExpenseAmt'] = doc[i].Return.ReturnData.IRS990.ExpenseAmt
                        objdetail['RevenueAmt'] = doc[i].Return.ReturnData.IRS990.RevenueAmt
                        objdetail['FormationYr'] = doc[i].Return.ReturnData.IRS990.FormationYr
                        // if (doc[i].Return.ReturnData.IRS990.ProgSrvcAccomActy3Grp.ExpenseAmt != undefined)
                        try {
                            objdetail['ProgSrvcAccomActyGrpExpenseAmt'] = doc[i].Return.ReturnData.IRS990.ProgSrvcAccomActy3Grp.ExpenseAmt

                        } catch (err) {
                            try {
                                objdetail['ProgSrvcAccomActyGrpExpenseAmt'] = doc[i].Return.ReturnData.IRS990.ProgSrvcAccomActy2Grp.ExpenseAmt

                            } catch (err) {
                                objdetail['ProgSrvcAccomActyGrpExpenseAmt'] = ""
                            }

                        }
                        try {
                            objdetail['ProgSrvcAccomActyGrpRevenueAmt'] = doc[i].Return.ReturnData.IRS990.ProgSrvcAccomActy3Grp.RevenueAmt

                        } catch (err) {
                            try {
                                objdetail['ProgSrvcAccomActyGrpRevenueAmt'] = doc[i].Return.ReturnData.IRS990.ProgSrvcAccomActy2Grp.RevenueAmt

                            } catch (err) {
                                objdetail['ProgSrvcAccomActyGrpRevenueAmt'] = ""
                            }

                        }
                        try {
                            objdetail['ProgSrvcAccomActyGrpDesc'] = doc[i].Return.ReturnData.IRS990.ProgSrvcAccomActy3Grp.Desc

                        } catch (err) {
                            try {
                                objdetail['ProgSrvcAccomActyGrpDesc'] = doc[i].Return.ReturnData.IRS990.ProgSrvcAccomActy2Grp.Desc

                            } catch (err) {
                                objdetail['ProgSrvcAccomActyGrpDesc'] = ""
                            }

                        }

                        csv_data.push(objdetail)



                    }
                    res.csv(csv_data)
                    //console.log(JSON.stringify(doc))
                }
            })


        }
    );



});
//
// /* GET home page. */
//
// router.get('/', function (req, res, next) {
//     MongoClient.connect(dbURI, function (err, db) {
//             if (err) {
//                 throw err
//             }
//
//             var cursor = db.collection('irs').find({"Return.ReturnHeader.Filer.EIN": "742661023"})
//             cursor.toArray(function (err, doc) {
//                 if (err) throw err
//
//                 if (doc != null) {
//                     //console.log(JSON.stringify(doc))
//                     res.json(doc)
//                 }
//             })
//
//
//         }
//     );
//
//
// });
//
// /**
//  * @swagger
//  * /api/organizationsInfo:
//  *   post:
//  *     tags:
//  *       - Get All Organization
//  *     description: get All organization
//  *     produces:
//  *       - application/json
//  *     parameters:
//  *       - name: offset
//  *         description: offset
//  *         in: path
//  *         required: true
//  *         type: integer
//  *       - name: size
//  *         description: size
//  *         in: path
//  *         required: true
//  *         type: integer
//  *     responses:
//  *       200:
//  *         description: count in number
//  *         schema:
//  *
//  */
// router.post('/organizationsInfo', function (req, res, next) {
//     var offset = req.body.offset
//     var size = req.body.size
//     var data = {}
//
//     //var sortBy = req.body.sortBy
//
//     MongoClient.connect(dbURI, function (err, db) {
//             var count = db.collection('npdirectory').find({}, {}).count(function (e, count) {
//                 console.log(count)
//                 data['count'] = count;
//             });
//             if (err) {
//                 throw err
//             }
//
//
//             var cursor = db.collection('npdirectory').find().skip(parseInt(offset)).limit(parseInt(size))
//             cursor.toArray(function (err, doc) {
//
//                 if (err) throw err
//                 var obj = []
//                 if (doc != null) {
//                     for (var i = 0; i < doc.length; i++) {
//                         var objdetail = {}
//                         objdetail['id'] = doc[i]._id
//
//                         try {
//                             objdetail['name'] = doc[i].info.BusinessName.BusinessNameLine1Txt
//
//                         } catch (err) {
//                             try {
//                                 objdetail['name'] = doc[i].info.BusinessName.BusinessNameLine1
//
//                             } catch (err) {
//                                 objdetail['name'] = doc[i].info.Name.BusinessNameLine1
//
//                             }
//
//                         }
//
//                         try {
//                             objdetail['addressLine1'] = doc[i].info.USAddress.AddressLine1
//
//                         } catch (err) {
//                             try {
//                                 objdetail['addressLine1'] = doc[i].info.USAddress.AddressLine1Txt
//                             } catch (err) {
//                                 objdetail['addressLine1'] = ''
//                             }
//
//
//                         }
//                         if (objdetail['cityName'] = doc[i].info.USAddress.City == undefined)
//                             objdetail['cityName'] = doc[i].info.USAddress.CityNm
//                         else
//                             objdetail['cityName'] = doc[i].info.USAddress.City
//                         if (objdetail['state'] = doc[i].info.USAddress.StateAbbreviationCd == undefined)
//                             objdetail['state'] = doc[i].info.USAddress.State
//                         else
//                             objdetail['state'] = doc[i].info.USAddress.StateAbbreviationCd
//                         if (objdetail['zip'] = doc[i].info.USAddress.ZIPCd == undefined)
//                             objdetail['zip'] = doc[i].info.USAddress.ZIPCode
//                         else
//                             objdetail['zip'] = doc[i].info.USAddress.ZIPCd
//                         if (objdetail['ratings'] = doc[i].rating == undefined)
//                             objdetail['ratings'] = 0
//                         else
//                             objdetail['ratings'] = doc[i].rating
//                         obj.push(objdetail)
//
//
//                     }
//
//                     //console.log(JSON.stringify(doc))
//                     data['data'] = obj
//                     res.json(data)
//                 }
//             })
//
//
//         }
//     );
// });
/**
 * @swagger
 * /api/searchByName/:name:
 *   post:
 *     tags:
 *       - SearchOrganization
 *     description: Search Organizations By Name
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: name
 *         description: name
 *         in: path
 *         required: true
 *         type: string
 *       - name: offset
 *         description: offset
 *         in: path
 *         required: true
 *         type: integer
 *       - name: size
 *         description: offset
 *         in: path
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: count in number
 *         schema:
 *
 */
router.post('/searchByName/', function (req, res, next) {
    var name = req.body.name
    var state = req.body.state
    var data = {}
    var offset = req.body.offset
    var size = req.body.size
    var query = new RegExp('.*' + name.toUpperCase() + '.*')

    var searchQuery = {}
    if (name == '') {
        if (state == '')
            searchQuery = {}
        else
            searchQuery = {
                "Return.ReturnHeader.Filer.BusinessName.BusinessNameLine1Txt": query,
                "Return.ReturnHeader.Filer.USAddress.StateAbbreviationCd": state
            }
    }
    else {
        if (state == '')
            searchQuery = {"Return.ReturnHeader.Filer.BusinessName.BusinessNameLine1Txt": query}
        else
            searchQuery = {
                "Return.ReturnHeader.Filer.BusinessName.BusinessNameLine1Txt": query,
                "Return.ReturnHeader.Filer.USAddress.StateAbbreviationCd": state
            }
    }
    /*if (name != '' && state == '') {
     searchQuery = {"Return.ReturnHeader.Filer.BusinessName.BusinessNameLine1Txt": query}
     }
     else {
     searchQuery = {
     "Return.ReturnHeader.Filer.BusinessName.BusinessNameLine1Txt": query,
     "Return.ReturnHeader.Filer.USAddress.StateAbbreviationCd": state
     }

     }*/
    console.log(searchQuery)
    /* MongoClient.connect(dbURI, function (err, db) {

     });
     */

    MongoClient.connect(dbURI, function (err, db) {
            if (err) {
                throw err
            }
            /* var count = db.collection('np990').find(searchQuery, {}).count(function (e, count) {
             console.log(count);

             data['count'] = (count);


             });
             */
            var Sequence = exports.Sequence || require('sequence').Sequence
                , sequence = Sequence.create()
                , err
                ;
            sequence.then(function (next) {
                var count = db.collection('np990').find(searchQuery, {}).count(function (e, count) {
                    console.log(count);

                    data['count'] = (count);
                    next(err, count)

                });

            }).then(function (next, count) {

                var cursor = db.collection('np990').find(searchQuery).skip(offset).limit(size)
                cursor.toArray(function (err, doc) {

                    if (err) throw err
                    var obj = []
                    if (doc != null) {
                        for (var i = 0; i < doc.length; i++) {
                            var objdetail = {}
                            objdetail['id'] = doc[i]._id

                            try {
                                objdetail['name'] = doc[i].Return.ReturnHeader.Filer.BusinessName.BusinessNameLine1Txt

                            } catch (err) {
                                try {
                                    objdetail['name'] = doc[i].info.BusinessName.BusinessNameLine1

                                } catch (err) {
                                    objdetail['name'] = doc[i].info.Name.BusinessNameLine1

                                }

                            }
                            if (doc[i].Return.ReturnHeader.Filer.USAddress.AddressLine1Txt == undefined)
                                objdetail['addressLine1'] = doc[i].Return.ReturnHeader.Filer.USAddress.AddressLine1
                            else
                                objdetail['addressLine1'] = doc[i].Return.ReturnHeader.Filer.USAddress.AddressLine1Txt
                            if (objdetail['cityName'] = doc[i].Return.ReturnHeader.Filer.USAddress.City == undefined)
                                objdetail['cityName'] = doc[i].Return.ReturnHeader.Filer.USAddress.CityNm
                            else
                                objdetail['cityName'] = doc[i].Return.ReturnHeader.Filer.USAddress.City
                            if (objdetail['state'] = doc[i].Return.ReturnHeader.Filer.USAddress.StateAbbreviationCd == undefined)
                                objdetail['state'] = doc[i].Return.ReturnHeader.Filer.USAddress.State
                            else
                                objdetail['state'] = doc[i].Return.ReturnHeader.Filer.USAddress.StateAbbreviationCd
                            if (objdetail['zip'] = doc[i].Return.ReturnHeader.Filer.USAddress.ZIPCd == undefined)
                                objdetail['zip'] = doc[i].Return.ReturnHeader.Filer.USAddress.ZIPCode
                            else
                                objdetail['zip'] = doc[i].Return.ReturnHeader.Filer.USAddress.ZIPCd
                            obj.push(objdetail)


                        }
                        data['data'] = obj
                        res.json(data)
                        next();
                    }
                })

            })


        }
    );


});
//
// router.post('/filter', function (req, res, next) {
//     var name = req.body.name
//     var city = req.body.name
//     var state = req.body.name
//     var data = {}
//     var query = new RegExp('.*' + name.toUpperCase() + '.*')
//     MongoClient.connect(dbURI, function (err, db) {
//         var count = db.collection('np990').find({"info.BusinessName.BusinessNameLine1Txt": query}, {}).count(function (e, count) {
//             console.log(count);
//             data['count'] = (count);
//         });
//     });
//
//     var offset = req.body.offset
//     var size = req.body.size
//     var query = new RegExp('.*' + name.toUpperCase() + '.*')
//     console.log(query)
//     MongoClient.connect(dbURI, function (err, db) {
//             if (err) {
//                 throw err
//             }
//
//
//             var cursor = db.collection('np990').find({"info.BusinessName.BusinessNameLine1Txt": query}).skip(offset).limit(size)
//             cursor.toArray(function (err, doc) {
//                 if (err) throw err
//                 var obj = []
//                 if (doc != null) {
//                     for (var i = 0; i < doc.length; i++) {
//                         var objdetail = {}
//                         objdetail['id'] = doc[i]._id
//
//                         try {
//                             objdetail['name'] = doc[i].info.BusinessName.BusinessNameLine1Txt
//
//                         } catch (err) {
//                             try {
//                                 objdetail['name'] = doc[i].info.BusinessName.BusinessNameLine1
//
//                             } catch (err) {
//                                 objdetail['name'] = doc[i].info.Name.BusinessNameLine1
//
//                             }
//
//                         }
//                         if (doc[i].info.USAddress.AddressLine1Txt == undefined)
//                             objdetail['addressLine1'] = doc[i].info.USAddress.AddressLine1
//                         else
//                             objdetail['addressLine1'] = doc[i].info.USAddress.AddressLine1Txt
//                         if (objdetail['cityName'] = doc[i].info.USAddress.City == undefined)
//                             objdetail['cityName'] = doc[i].info.USAddress.CityNm
//                         else
//                             objdetail['cityName'] = doc[i].info.USAddress.City
//                         if (objdetail['state'] = doc[i].info.USAddress.StateAbbreviationCd == undefined)
//                             objdetail['state'] = doc[i].info.USAddress.State
//                         else
//                             objdetail['state'] = doc[i].info.USAddress.StateAbbreviationCd
//                         if (objdetail['zip'] = doc[i].info.USAddress.ZIPCd == undefined)
//                             objdetail['zip'] = doc[i].info.USAddress.ZIPCode
//                         else
//                             objdetail['zip'] = doc[i].info.USAddress.ZIPCd
//                         obj.push(objdetail)
//
//
//                     }
//                     data['data'] = obj
//                     res.json(data)
//
//
//                 }
//             })
//
//
//         }
//     );
//
//
// });
// /**
//  * @swagger
//  * /api//organization/:id:
//  *   get:
//  *     tags:
//  *       - GetOrganization
//  *     description: Returns Organization Count
//  *     produces:
//  *       - application/json
//  *     parameters:
//  *       - name: id
//  *         description: Organization id
//  *         in: path
//  *         required: true
//  *         type: string
//  *     responses:
//  *       200:
//  *         description: count in number
//  *         schema:
//  *
//  */
//
// router.get('/organization/:id', function (req, res, next) {
//     var id = req.params.id
//     var ObjectId = require('mongodb').ObjectId;
//     var o_id = new ObjectId(id);
//
//     //var query = new RegExp('.*' + name.toUpperCase() + '.*')
//     // console.log(query)
//     MongoClient.connect(dbURI, function (err, db) {
//             if (err) {
//                 throw err
//             }
//
//             var cursor = db.collection('np990').find({"_id": o_id}, {})
//             cursor.toArray(function (err, doc) {
//                 if (err) throw err
//                 if (doc != null) {
//                     for (var i = 0; i < doc.length; i++) {
//                         var objdetail = {}
//                         objdetail['id'] = doc[i]._id
//
//                         objdetail['ein'] = doc[i].info.EIN
//                         try {
//                             objdetail['name'] = doc[i].info.BusinessName.BusinessNameLine1Txt
//
//                         } catch (err) {
//                             try {
//                                 objdetail['name'] = doc[i].info.BusinessName.BusinessNameLine1
//
//                             } catch (err) {
//                                 objdetail['name'] = doc[i].info.Name.BusinessNameLine1
//
//                             }
//
//                         }
//                         if (doc[i].info.USAddress.AddressLine1Txt == undefined)
//                             objdetail['addressLine1'] = doc[i].info.USAddress.AddressLine1
//                         else
//                             objdetail['addressLine1'] = doc[i].info.USAddress.AddressLine1Txt
//                         if (objdetail['cityName'] = doc[i].info.USAddress.City == undefined)
//                             objdetail['cityName'] = doc[i].info.USAddress.CityNm
//                         else
//                             objdetail['cityName'] = doc[i].info.USAddress.City
//                         if (objdetail['state'] = doc[i].info.USAddress.StateAbbreviationCd == undefined)
//                             objdetail['state'] = doc[i].info.USAddress.State
//                         else
//                             objdetail['state'] = doc[i].info.USAddress.StateAbbreviationCd
//                         if (objdetail['zip'] = doc[i].info.USAddress.ZIPCd == undefined)
//                             objdetail['zip'] = doc[i].info.USAddress.ZIPCode
//                         else
//                             objdetail['zip'] = doc[i].info.USAddress.ZIPCd
//                         objdetail['website'] = doc[i].website
//                         objdetail['expenseAmt'] = doc[i].expenseAmt
//                         objdetail['taxYear'] = doc[i].taxYear
//                         objdetail['rulingDate'] = doc[i].rulingdate
//                         objdetail['missionDescription'] = doc[i].missionDescription
//                         objdetail['returnType'] = doc[i].returnType
//                         objdetail['businessOfficerPersonName'] = doc[i].businessOfficerGroup.PersonNm
//                         objdetail['businessOfficerPersonTitle'] = doc[i].businessOfficerGroup.PersonTitleTxt
//                         objdetail['businessOfficerPersonPhone'] = doc[i].businessOfficerGroup.PhoneNum
//                         objdetail['businessOfficerPersonSignatureDate'] = doc[i].businessOfficerGroup.SignatureDt
//
//
//                         res.json(objdetail)
//
//
//                     }
//                     //console.log(JSON.stringify(doc))
//                 }
//             })
//
//
//         }
//     );
//
//
// });

router.get('/organization990/:id', function (req, res, next) {
    var id = req.params.id
    var ObjectId = require('mongodb').ObjectId;
    var o_id = new ObjectId(id);

    //var query = new RegExp('.*' + name.toUpperCase() + '.*')
    // console.log(query)
    MongoClient.connect(dbURI, function (err, db) {
            if (err) {
                throw err
            }

            var cursor = db.collection('np990').find({"_id": o_id}, {})
            cursor.toArray(function (err, doc) {
                if (err) throw err
                if (doc != null) {
                    for (var i = 0; i < doc.length; i++) {
                        var objdetail = {}
                        objdetail['id'] = doc[i]._id

                        objdetail['ein'] = doc[i].Return.ReturnHeader.Filer.EIN
                        try {
                            objdetail['name'] = doc[i].Return.ReturnHeader.Filer.BusinessName.BusinessNameLine1Txt

                        } catch (err) {
                            try {
                                objdetail['name'] = doc[i].info.BusinessName.BusinessNameLine1

                            } catch (err) {
                                objdetail['name'] = doc[i].info.Name.BusinessNameLine1

                            }

                        }
                        if (doc[i].Return.ReturnHeader.Filer.USAddress.AddressLine1Txt == undefined)
                            objdetail['addressLine1'] = doc[i].Return.ReturnHeader.Filer.USAddress.AddressLine1Txt
                        else
                            objdetail['addressLine1'] = doc[i].Return.ReturnHeader.Filer.USAddress.AddressLine1Txt
                        if (objdetail['cityName'] = doc[i].Return.ReturnHeader.Filer.USAddress.City == undefined)
                            objdetail['cityName'] = doc[i].Return.ReturnHeader.Filer.USAddress.CityNm
                        else
                            objdetail['cityName'] = doc[i].Return.ReturnHeader.Filer.USAddress.City
                        if (objdetail['state'] = doc[i].Return.ReturnHeader.Filer.USAddress.StateAbbreviationCd == undefined)
                            objdetail['state'] = doc[i].Return.ReturnHeader.Filer.USAddress.StateAbbreviationCd
                        else
                            objdetail['state'] = doc[i].Return.ReturnHeader.Filer.USAddress.StateAbbreviationCd
                        if (objdetail['zip'] = doc[i].Return.ReturnHeader.Filer.USAddress.ZIPCd == undefined)
                            objdetail['zip'] = doc[i].Return.ReturnHeader.Filer.USAddress.ZIPCd
                        else
                            objdetail['zip'] = doc[i].Return.ReturnHeader.Filer.USAddress.ZIPCd
                        /*objdetail['website'] = doc[i].website
                         objdetail['expenseAmt'] = doc[i].expenseAmt
                         objdetail['taxYear'] = doc[i].taxYear
                         objdetail['rulingDate'] = doc[i].rulingdate
                         objdetail['missionDescription'] = doc[i].missionDescription*/
                        objdetail['returnType'] = doc[i].Return.ReturnHeader.returnType
                        objdetail['businessOfficerPersonName'] = doc[i].Return.ReturnHeader.BusinessOfficerGrp.PersonNm
                        objdetail['businessOfficerPersonTitle'] = doc[i].Return.ReturnHeader.BusinessOfficerGrp.PersonTitleTxt
                        objdetail['businessOfficerPersonPhone'] = doc[i].Return.ReturnHeader.BusinessOfficerGrp.PhoneNum
                        objdetail['businessOfficerPersonSignatureDate'] = doc[i].Return.ReturnHeader.BusinessOfficerGrp.SignatureDt
                        objdetail['EmployeeCnt'] = doc[i].Return.ReturnData.IRS990.EmployeeCnt
                        objdetail['TotalVolunteersCnt'] = doc[i].Return.ReturnData.IRS990.TotalVolunteersCnt
                        objdetail['PYTotalRevenueAmt'] = doc[i].Return.ReturnData.IRS990.PYTotalRevenueAmt
                        objdetail['CYTotalRevenueAmt'] = doc[i].Return.ReturnData.IRS990.CYTotalRevenueAmt
                        objdetail['CYTotalFundraisingExpenseAmt'] = doc[i].Return.ReturnData.IRS990.CYTotalFundraisingExpenseAmt
                        objdetail['PYContributionsGrantsAmt'] = doc[i].Return.ReturnData.IRS990.PYContributionsGrantsAmt
                        objdetail['CYContributionsGrantsAmt'] = doc[i].Return.ReturnData.IRS990.CYContributionsGrantsAmt
                        objdetail['PYProgramServiceRevenueAmt'] = doc[i].Return.ReturnData.IRS990.PYProgramServiceRevenueAmt
                        objdetail['CYProgramServiceRevenueAmt'] = doc[i].Return.ReturnData.IRS990.CYProgramServiceRevenueAmt
                        objdetail['PYInvestmentIncomeAmt'] = doc[i].Return.ReturnData.IRS990.PYInvestmentIncomeAmt
                        objdetail['CYInvestmentIncomeAmt'] = doc[i].Return.ReturnData.IRS990.CYInvestmentIncomeAmt
                        objdetail['PYOtherRevenueAmt'] = doc[i].Return.ReturnData.IRS990.PYOtherRevenueAmt
                        objdetail['CYOtherRevenueAmt'] = doc[i].Return.ReturnData.IRS990.CYOtherRevenueAmt
                        objdetail['PYSalariesCompEmpBnftPaidAmt'] = doc[i].Return.ReturnData.IRS990.PYSalariesCompEmpBnftPaidAmt
                        objdetail['CYSalariesCompEmpBnftPaidAmt'] = doc[i].Return.ReturnData.IRS990.CYSalariesCompEmpBnftPaidAmt
                        objdetail['PYTotalProfFndrsngExpnsAmt'] = doc[i].Return.ReturnData.IRS990.PYTotalProfFndrsngExpnsAmt
                        objdetail['CYTotalProfFndrsngExpnsAmt'] = doc[i].Return.ReturnData.IRS990.CYTotalProfFndrsngExpnsAmt
                        objdetail['CYTotalFundraisingExpenseAmt'] = doc[i].Return.ReturnData.IRS990.CYTotalFundraisingExpenseAmt
                        objdetail['CYTotalFundraisingExpenseAmt'] = doc[i].Return.ReturnData.IRS990.CYTotalFundraisingExpenseAmt
                        objdetail['missionDescription'] = doc[i].Return.ReturnData.IRS990.MissionDesc
                        objdetail['ExpenseAmt'] = doc[i].Return.ReturnData.IRS990.ExpenseAmt
                        objdetail['RevenueAmt'] = doc[i].Return.ReturnData.IRS990.RevenueAmt
                        objdetail['FormationYr'] = doc[i].Return.ReturnData.IRS990.FormationYr
                        // if (doc[i].Return.ReturnData.IRS990.ProgSrvcAccomActy3Grp.ExpenseAmt != undefined)
                        try {
                            objdetail['ProgSrvcAccomActyGrpExpenseAmt'] = doc[i].Return.ReturnData.IRS990.ProgSrvcAccomActy3Grp.ExpenseAmt

                        } catch (err) {
                            try {
                                objdetail['ProgSrvcAccomActyGrpExpenseAmt'] = doc[i].Return.ReturnData.IRS990.ProgSrvcAccomActy2Grp.ExpenseAmt

                            } catch (err) {
                                objdetail['ProgSrvcAccomActyGrpExpenseAmt'] = ""
                            }

                        }
                        try {
                            objdetail['ProgSrvcAccomActyGrpRevenueAmt'] = doc[i].Return.ReturnData.IRS990.ProgSrvcAccomActy3Grp.RevenueAmt

                        } catch (err) {
                            try {
                                objdetail['ProgSrvcAccomActyGrpRevenueAmt'] = doc[i].Return.ReturnData.IRS990.ProgSrvcAccomActy2Grp.RevenueAmt

                            } catch (err) {
                                objdetail['ProgSrvcAccomActyGrpRevenueAmt'] = ""
                            }

                        }
                        try {
                            objdetail['ProgSrvcAccomActyGrpDesc'] = doc[i].Return.ReturnData.IRS990.ProgSrvcAccomActy3Grp.Desc

                        } catch (err) {
                            try {
                                objdetail['ProgSrvcAccomActyGrpDesc'] = doc[i].Return.ReturnData.IRS990.ProgSrvcAccomActy2Grp.Desc

                            } catch (err) {
                                objdetail['ProgSrvcAccomActyGrpDesc'] = ""
                            }

                        }


                        res.json(objdetail)


                    }
                    //console.log(JSON.stringify(doc))
                }
            })


        }
    );


});


router.post('/aggregate/count', function (req, res, next) {

    var type = req.body.type
    var formType = req.body.formType

    if (type == 'state' && formType == '990')
        var query = {$group: {_id: 'Return.ReturnHeader.Filer.USAddress.StateAbbreviationCd', count: {$sum: 1}}}
    console.log(query)

    MongoClient.connect(dbURI, function (err, db) {
            if (err) {
                throw err
            }

            var cursor = db.collection('np990').aggregate(query)
            cursor.toArray(function (err, doc) {
                if (err) throw err

                if (doc != null) {

                    res.json(doc)
                }
            })


        }
    );


});

router.get('/nonProfitByEIN/:EIN', function (req, res, next) {
    "db.np990.aggregate( {$group : { _id : 'Return.ReturnHeader.Filer.USAddress.StateAbbreviationCd', count : {$sum : 1}}})"
    var EIN = req.params.EIN

    var query = new RegExp(EIN.toUpperCase() + '.*')
    console.log(query)

    MongoClient.connect(dbURI, function (err, db) {
            if (err) {
                throw err
            }

            var cursor = db.collection('irs').aggregate({"Return.ReturnHeader.Filer.EIN": EIN})
            cursor.toArray(function (err, doc) {
                if (err) throw err

                if (doc != null) {
                    //console.log(JSON.stringify(doc))
                    res.json(doc)
                }
            })


        }
    );


});

/**
 * @swagger
 * /api/organizationCount:
 *   get:
 *     tags:
 *       - OrganizationCount
 *     description: Returns Organization Count
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: count in number
 *         schema:
 *
 */

router.get('/organizationCount', function (req, res, next) {
    var data = {}
    MongoClient.connect(dbURI, function (err, db) {
        var count = db.collection('npdirectory').find({}, {}).count(function (e, count) {
            console.log(count);
            data['count'] = (count);
            res.json(count)
        });
    });


});


router.get('/organizationSearchCount/:name', function (req, res, next) {
    var name = req.params.name
    var data = {}
    var query = new RegExp('.*' + name.toUpperCase() + '.*')
    MongoClient.connect(dbURI, function (err, db) {
        var count = db.collection('npdirectory').find({"info.BusinessName.BusinessNameLine1Txt": query}, {}).count(function (e, count) {
            console.log(count);
            data['count'] = (count);
            res.json(count)
        });
    });


});


router.get('/test', function (req, res, next) {
    var name = 's'
    var data = {}
    var query = new RegExp('.*' + name.toUpperCase() + '.*')
    MongoClient.connect(dbURI, function (err, db) {
        var count = db.collection('npdirectory').find({"info.BusinessName.BusinessNameLine1Txt": query}, {}).count(function (e, count) {
            console.log(count);
            data['count'] = (count);
            data['name'] = 'name'
            res.json(data)
        });
    });


});


router.post('/updateOrganization', function (req, res, next) {
    var id = req.body.id
    var ObjectId = require('mongodb').ObjectId;
    var o_id = new ObjectId(id);
    //var sortBy = req.body.sortBy

    MongoClient.connect(dbURI, function (err, db) {


            db.collection('npdirectory').update({"_id": o_id}, {$set: {'rating': 7}}, function (err, result) {
                if (err) throw err;
            })


        }
    );
    res.json('success')

});


router.post('/organizations990', function (req, res, next) {
    var offset = req.body.offset
    var size = req.body.size
    var state = req.body.state
    var data = {}
    var query = {}
    if (state == '') {
        query = {}
    }
    else query = {"Return.ReturnHeader.Filer.USAddress.StateAbbreviationCd": state}
    console.log(query)
    //var sortBy = req.body.sortBy

    MongoClient.connect(dbURI, function (err, db) {
            var count = db.collection('np990').find(query, {}).count(function (e, count) {
                console.log(count)
                data['count'] = count;
            });
            if (err) {
                throw err
            }


            var cursor = db.collection('np990').find(query).skip(parseInt(offset)).limit(parseInt(size))
            cursor.toArray(function (err, doc) {

                if (err) throw err
                var obj = []
                if (doc != null) {
                    for (var i = 0; i < doc.length; i++) {
                        var objdetail = {}
                        objdetail['id'] = doc[i]._id

                        try {
                            objdetail['name'] = doc[i].Return.ReturnHeader.Filer.BusinessName.BusinessNameLine1Txt

                        } catch (err) {
                            objdetail['name'] = ''
                        }

                        try {
                            objdetail['addressLine1'] = doc[i].info.USAddress.AddressLine1

                        } catch (err) {
                            try {
                                objdetail['addressLine1'] = doc[i].Return.ReturnHeader.Filer.USAddress.AddressLine1Txt
                            } catch (err) {
                                objdetail['addressLine1'] = ''
                            }


                        }
                        if (objdetail['cityName'] = doc[i].Return.ReturnHeader.Filer.USAddress.City == undefined)
                            objdetail['cityName'] = doc[i].Return.ReturnHeader.Filer.USAddress.CityNm
                        else
                            objdetail['cityName'] = doc[i].Return.ReturnHeader.Filer.USAddress.City
                        if (objdetail['state'] = doc[i].Return.ReturnHeader.Filer.USAddress.StateAbbreviationCd == undefined)
                            objdetail['state'] = doc[i].Return.ReturnHeader.Filer.USAddress.StateAbbreviationCd
                        else
                            objdetail['state'] = doc[i].Return.ReturnHeader.Filer.USAddress.StateAbbreviationCd
                        if (objdetail['zip'] = doc[i].Return.ReturnHeader.Filer.USAddress.ZIPCd == undefined)
                            objdetail['zip'] = doc[i].Return.ReturnHeader.Filer.USAddress.ZIPCd
                        else
                            objdetail['zip'] = doc[i].Return.ReturnHeader.Filer.USAddress.ZIPCd
                        if (objdetail['ratings'] = doc[i].rating == undefined)
                            objdetail['ratings'] = 0
                        else
                            objdetail['ratings'] = doc[i].rating
                        obj.push(objdetail)


                    }

                    //console.log(JSON.stringify(doc))
                    data['data'] = obj
                    res.json(data)
                }
            })


        }
    );
});
module.exports = router;
